public class SavingAccount extends Account {
	
	boolean withdraw = false;
	public SavingAccount(int id){
		super(id, 0);
	}

	@Override
	public int withDrawMoney(int suma) {
		
		if(withdraw){
			return 0;
		}
		int rez;
		if ((getSold() + suma) < 0)
			rez = -1;
		else {
			rez = 0;
			setSold(getSold() - suma);
			//sold=sold-suma;

		}
		withdraw = true;
		return rez;

	}

	@Override
	public int depositMoney(int suma) {
		if(suma<1000)
		sold+=suma;
		return suma;
	}
}