import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Bank implements Serializable {

	private Map<Person, ArrayList<Account>> map = new HashMap<Person, ArrayList<Account>>();

	public boolean isMapConsistent() {
		for (Map.Entry<Person, ArrayList<Account>> entry : map.entrySet()) 
			if (entry.getValue().isEmpty())
				return false;
		return true;
	}
	
	public Map<Person, ArrayList<Account>> getMap() {
		return map;
	}

	public void setMap(Map<Person, ArrayList<Account>> map) {
		this.map = map;
	}
	
	public void adaugaClient(Person p, ArrayList<Account> conturi) {
		assert isMapConsistent();
		map.put(p, conturi);
		assert isMapConsistent();
	}


	public void stergeClient(Person p) {
		assert isMapConsistent();
		map.remove(p);
		assert isMapConsistent();

	}


	public void adaugaCont(Person p, Account a) {
		assert isMapConsistent();
		if (map.get(p)==null)
			adaugaClient(p, new ArrayList<Account>());
			
		if (!map.get(p).contains(a))
			map.get(p).add(a);
		assert isMapConsistent();
	}


	public void stergeCont(Person p, Account a) {
		assert isMapConsistent();
		map.get(p).remove(a);
		assert isMapConsistent();

	}

	public void stringDisplay() {
		for (Entry<Person, ArrayList<Account>> entry : this.getMap().entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}
	}
	
	public ArrayList<Object[]> tableDisplay()
	{
		ArrayList<Object[]> r = new ArrayList<Object[]>();
		for (Entry<Person, ArrayList<Account>> entry : map.entrySet()) {
			{
				for (Account acc: entry.getValue())
				{
					Object[] details = new Object[5];
					details[0]= entry.getKey().getName();
					details[1]= entry.getKey().getAddress();
					details[2]= acc.getType();
					details[3]= acc.getId();
					details[4]= acc.getSold();
					r.add(details);
				}
					
			}
		}
		return r;
	}

	public void addBani(Person p, Account a, int suma) {
		assert isMapConsistent();
		int c = map.get(p).indexOf(a);
		map.get(p).get(c).depositMoney(suma);
		assert isMapConsistent();

	}


	public void removeBani(Person b, Account a, int suma) {
		assert isMapConsistent();
		int c = map.get(b).indexOf(a);
		map.get(b).get(c).withDrawMoney(suma);
		assert isMapConsistent();

	}


	public void serialization() {
		try {
			FileOutputStream fileOut = new FileOutputStream("banca.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(map);
			out.close();
			fileOut.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void deserialization() {
		try {
			FileInputStream fileIn = new FileInputStream("banca.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			HashMap<Person, ArrayList<Account>> readObject = (HashMap<Person, ArrayList<Account>>) in.readObject();
			map=readObject;
			in.close();
			fileIn.close();
			
		} catch (IOException i) {
			System.out.println("Cannot load bank info");;
			return;
		} catch (ClassNotFoundException e) {
			System.out.println("ClassUnfound");
			e.printStackTrace();
		}
	}

//2 metode abstracte cu withdraw/add si savin in subclasele saving si spending Account
}
