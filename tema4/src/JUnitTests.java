import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;

public class JUnitTests {
	Bank banca=new Bank();
	@Test
	public void testInsertAccount() {
		Person p=new Person("Ionescu Ion","Cluj-Napoca");
		SavingAccount sav=new SavingAccount(234);
		SavingAccount spd=new SavingAccount(54);
		ArrayList<Account> cont = new ArrayList<Account>();
		cont.add(sav);
		cont.add(spd);
		
		banca.adaugaClient(p, cont);
		
		assertEquals(banca.getMap().get(p),cont);
	}

	@Test
	public void testFail() {
		fail("Not yet implemented");
	}
}
