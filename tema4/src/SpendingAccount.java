public class SpendingAccount extends Account {

	public SpendingAccount(int id) {
		super(id, 1);
	}

	@Override
	public int withDrawMoney(int suma) {
		int rez;
		if ((getSold() - suma) > 0)
			rez = 0;
		else {
			rez = -1;
			setSold(getSold() - suma);
			//sold=sold-suma;

		}
		return rez;

	}

	@Override
	public int depositMoney(int suma) {
		if(suma<1000)
		sold += suma;
		return suma;
	}

}

// trebuie sa stie in interfata withdraw si depositu