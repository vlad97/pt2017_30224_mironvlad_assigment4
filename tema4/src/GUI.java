import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GUI {
	JFrame frame;

	private Bank bank;

	DefaultTableModel model;
	private JTable tableData;

	private JTextField textID;
	private JTextField textName;
	private JTextField textAddress;
	private JTextField textNr;
	private JTextField textSum;
	private JButton btnAdd;
	private JButton btnDelete;

	private void updateTable()
	{
		model.fireTableRowsDeleted(0, model.getRowCount());
		model.setRowCount(0);

		for (Object[] row: bank.tableDisplay())
			model.addRow(row);


		tableData.setModel(model);	
	}

	public GUI(Bank b) {
		bank=b;
		model = new DefaultTableModel(new Object[][] { {} },
				new Object[] { "Name", "Address", "AccountType", "AccountNumber", "AccountBalance" })
		{
			private static final long serialVersionUID = 1L;
			public boolean isCellEditable(int row, int column) {
				//all cells false
				return false;
			}
		};

		frame = new JFrame();

		frame.setBounds(100, 100, 850, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		tableData = new JTable(model);
		updateTable();
		JScrollPane tb = new JScrollPane(tableData);
		tb.setBounds(40, 40, 750, 250);
		tb.setVisible(true);
		frame.getContentPane().add(tb);


		btnAdd = new JButton("AddClient");
		btnAdd.setBounds(50, 450, 150, 23);
		frame.getContentPane().add(btnAdd);

		btnDelete = new JButton("DeleteClient");
		btnDelete.setBounds(50, 480, 150, 23);
		frame.getContentPane().add(btnDelete);

		JButton btnAddacc = new JButton("Add Account");
		btnAddacc.setBounds(250, 450, 150, 23);
		frame.getContentPane().add(btnAddacc);

		JButton btnDelacc = new JButton("Delete Acccount");
		btnDelacc.setBounds(250, 480, 150, 23);
		frame.getContentPane().add(btnDelacc);		
		
		JLabel lblNume = new JLabel("Name");
		lblNume.setBounds(30, 319, 100, 14);
		frame.getContentPane().add(lblNume);


		JLabel lblAdr = new JLabel("Address");
		lblAdr.setBounds(30, 350, 100, 14);
		frame.getContentPane().add(lblAdr);


		JLabel lblType = new JLabel("AccountType");
		lblType.setBounds(30, 381, 100, 14);
		frame.getContentPane().add(lblType);


		JLabel lblNr = new JLabel("AccountNumber");
		lblNr.setBounds(30, 412, 100, 14);
		frame.getContentPane().add(lblNr);


		JLabel lblCash = new JLabel("Amount");
		lblCash.setBounds(400, 340, 100, 14);
		frame.getContentPane().add(lblCash);			

		textName = new JTextField();
		textName.setBounds(100, 316, 150, 20);
		frame.getContentPane().add(textName);
		textName.setColumns(10);

		textAddress = new JTextField();
		textAddress.setBounds(100, 347, 150, 20);
		frame.getContentPane().add(textAddress);
		textAddress.setColumns(10);

		textID = new JTextField();
		textID.setBounds(150, 378, 150, 20);
		frame.getContentPane().add(textID);
		textID.setColumns(10);

		textNr = new JTextField();
		textNr.setText("");
		textNr.setBounds(150, 409, 150, 20);
		frame.getContentPane().add(textNr);
		textNr.setColumns(10);

		textSum = new JTextField();
		textSum.setText("");
		textSum.setBounds(450, 337, 150, 20);
		frame.getContentPane().add(textSum);
		textSum.setColumns(10);		
		
		
		JButton btnAddMoney = new JButton("Add Money");
		btnAddMoney.setBounds(450, 360, 130, 23);
		frame.getContentPane().add(btnAddMoney);

		JButton btnRemMoney = new JButton("Withdraw Money");
		btnRemMoney.setBounds(450, 390, 130, 23);
		frame.getContentPane().add(btnRemMoney);


		btnAdd.addActionListener(new ActionListener() { // Adaugare Client 
			public void actionPerformed(ActionEvent arg0) {

				Person plus=new Person(textName.getText(),textAddress.getText());
				ArrayList<Account> conturi=new ArrayList<Account>();
				SavingAccount primuls;
				SpendingAccount primuld;

				int x=Integer.parseInt(textID.getText());

				if (x==0)
				{
					primuls = new SavingAccount(Integer.parseInt(textNr.getText()));
					conturi.add(primuls);
				}
				else
				{

					primuld = new SpendingAccount(Integer.parseInt(textNr.getText()));
					conturi.add(primuld);
				}

				bank.adaugaClient(plus, conturi);

				updateTable();
				bank.serialization();
			}
		});

		btnAddacc.addActionListener(new ActionListener() {  // Adaugare Cont
			public void actionPerformed(ActionEvent arg0) {		
				Person plus=new Person(textName.getText(),textAddress.getText());			
				ArrayList<Account> conturi=new ArrayList<Account>();		
				SavingAccount primuls;
				SpendingAccount primuld;			
				int x=Integer.parseInt(textID.getText());		
				if (x==0)
				{
					primuls=new SavingAccount(Integer.parseInt(textNr.getText()));
					conturi.add(primuls);
					bank.adaugaCont(plus, primuls);
				}
				else
				{				
					primuld = new SpendingAccount(Integer.parseInt(textNr.getText()));
					conturi.add(primuld);
					bank.adaugaCont(plus, primuld);
				}					
				updateTable();			
				bank.serialization();		
			}
		});

		tableData.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int i = tableData.getSelectedRow();
				textName.setText(model.getValueAt(i, 0).toString());
				textAddress.setText(model.getValueAt(i, 1).toString());
				textID.setText(model.getValueAt(i, 2).toString());
				textNr.setText(model.getValueAt(i, 3).toString());

			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person plus=new Person(textName.getText(), textAddress.getText());
				bank.stergeClient(plus);
				bank.serialization();	
				updateTable();
			}
		});

		btnDelacc.addActionListener(new ActionListener() { //stergere cont
			public void actionPerformed(ActionEvent arg0) {
				Person plus=new Person(textName.getText(),textAddress.getText());
				ArrayList<Account> conturi=new ArrayList<Account>();
				SavingAccount primuls;
				SpendingAccount primuld;

				int x=Integer.parseInt(textID.getText());

				if (x==0)
				{
					primuls = new SavingAccount(Integer.parseInt(textNr.getText()));
					bank.stergeCont(plus, primuls);
				}
				else
				{				
					primuld = new SpendingAccount(Integer.parseInt(textNr.getText()));
					bank.stergeCont(plus, primuld);
				}
				bank.serialization();
				updateTable();
			}
		});

		btnAddMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person plus=new Person(textName.getText(),textAddress.getText());
				ArrayList<Account> conturi=new ArrayList<Account>();
				SavingAccount primuls;
				SpendingAccount primuld;			
				int x=Integer.parseInt(textID.getText());

				if (x==0)
				{
					primuls = new SavingAccount(Integer.parseInt(textNr.getText()));
					bank.addBani(plus,primuls,Integer.parseInt(textSum.getText()));
				}
				else
				{
					primuld = new SpendingAccount(Integer.parseInt(textNr.getText()));
					bank.addBani(plus,primuld,Integer.parseInt(textSum.getText()));
				}									
				bank.serialization();
				updateTable();
			}
		});

		btnRemMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person plus=new Person(textName.getText(),textAddress.getText());
				ArrayList<Account> conturi=new ArrayList<Account>();
				SpendingAccount primuld;			
				int x=Integer.parseInt(textID.getText());

				if (x==1)
				{			
					primuld = new SpendingAccount(Integer.parseInt(textNr.getText()));
					bank.removeBani(plus,primuld,Integer.parseInt(textSum.getText()));
				}
				bank.serialization();
				updateTable();
			}
		});

		frame.addWindowListener(new WindowAdapter() {

			public void WindowClosing(WindowEvent event) {
				bank.serialization();
				frame.dispose();
			}

		});
		
		frame.setVisible(true);
	}
}
