import java.io.Serializable;

public class Person implements Serializable {
	private String name;
	private String address;

	public Person(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		return name.hashCode() ^ address.hashCode();
	};
	
	@Override
	public boolean equals(Object obj)
	{
		return ((Person)obj).name.equals(name) && ((Person)obj).address.equals(address);
	}
	
}