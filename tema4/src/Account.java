import java.io.Serializable;

public abstract class Account implements Serializable {
	protected int id;
	protected int sold;
	protected int type;
	
	public Account(int id, int type){
		this.id = id;
		this.sold = 0;
		this.type = type;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id=id;
		
	}

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold=sold;
	}
	
	public int getType(){
		return type;
	}
	
	public void setType(int n){
		this.type=n;
	}
	

	@Override
	public boolean equals(Object obj)
	{
		return ((Account)obj).id == id;
	}
	
	public abstract int withDrawMoney(int suma);
	public abstract int depositMoney(int suma);
	
	@Override
	public int hashCode()
	{
		return id;
	}


}